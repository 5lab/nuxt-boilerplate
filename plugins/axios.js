export default function({ $axios, redirect, isDev, app, store }) {
  $axios.onRequest(config => {
    store.commit('SET_LOADING', true)
    if (store.state.auth.token) {
      // $axios.setToken(store.state.auth.token)
      config.headers.common[
        'Authorization'
      ] = `Bearer ${store.state.auth.token}`
    }
    // isDev && console.log(`============================`)
    // isDev && console.log(`Making request to ${config.baseURL}${config.url}`)
    // isDev &&
    // console.log(`–––––– token: ${config.headers.common['Authorization']}`)
    return config
  })
  $axios.onResponse(response => {
    store.commit('SET_LOADING', false)
  })
  $axios.onRequestError(err => {
    store.commit('SET_LOADING', false)
  })
  $axios.onResponseError(err => {
    store.commit('SET_LOADING', false)
  })
  $axios.onError(error => {
    console.log('axios error')
    store.commit('SET_LOADING', false)
  })
}
