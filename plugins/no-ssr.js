import Vue from 'vue'
import WebFont from 'webfontloader'
import VueLazyload from 'vue-lazyload'
import Transitions from 'vue2-transitions'
import Affix from 'vue-affix'
import Scrollactive from 'vue-scrollactive'
import LightBox from 'vue-image-lightbox'
require('vue-image-lightbox/dist/vue-image-lightbox.min.css')
const VueAwesomeSwiper = require('vue-awesome-swiper/dist/ssr')
Vue.use(VueAwesomeSwiper)
Vue.use(VueLazyload, {
  preLoad: 1.3,
  // error: 'dist/error.png',
  loading: 'images/loading.gif',
  attempt: 1
})
Vue.use(Affix)
Vue.use(Transitions)
Vue.use(Scrollactive)
Vue.component('LightBox', LightBox)

import * as VueGoogleMaps from '~/node_modules/vue2-google-maps/src/main'
Vue.use(VueGoogleMaps, {
  load: {
    autobindAllEvents: true,
    key: 'AIzaSyDSW5Lj4B7dzUbpVm3WKCKYhfIYdn4e2pw',
    libraries: 'places' // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)
  }
})

export default async function({ store, redirect }) {
  // Fetch all pages
  WebFont.load({
    google: {
      // families: ['Barlow+Condensed:400,600,700', 'Noto Sans']
    }
  })
}
