const pkg = require('./package')

module.exports = {
  env: {
    BUILD_ID: require('child_process')
      .execSync('git rev-parse --short HEAD')
      .toString()
      .trim()
  },
  mode: 'universal',
  debug: true,
  /*
   ** Headers of the page
   */
  head: {
    title: pkg.name,
    meta: [
      {
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content:
          'width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no'
      },
      {
        hid: 'description',
        name: 'description',
        content: pkg.description
      }
    ],
    script: [
      {
        src:
          'https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/7.2.5/polyfill.min.js'
      },
      {
        src: '/js/offline.min.js',
        body: true
      }
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: false,

  /*
   ** Global CSS
   */
  css: [
    {
      src: 'assets/styles/main'
    }
  ],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    {
      src: '~/plugins/mixins.js'
    },
    {
      src: '~/plugins/axios.js'
    },
    {
      src: '~/plugins/ssr.js'
    },
    {
      src: '~/plugins/no-ssr.js',
      mode: 'client'
    }
  ],
  render: {
    resourceHints: false
  },
  /*
   ** Nuxt.js modules
   */
  modules: [
    [
      'vue-scrollto/nuxt',
      {
        duration: 300
      }
    ],
    ['@nuxtjs/moment', ['th']],
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    // Doc: https://buefy.github.io/#/documentation
    [
      'nuxt-buefy',
      {
        css: false,
        materialDesignIcons: false,
        defaultIconPack: 'fa'
      }
    ],
    'cookie-universal-nuxt',
    [
      '@nuxtjs/google-tag-manager',
      {
        id: pkg.gtmId,
        pageTracking: false
      }
    ]
  ],
  router: {
    middleware: ['router']
  },
  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    credentials: false,
    debug: false,
    https: true,
    port: 443,
    host: pkg.apiUrl
  },

  /*
   ** Build configuration
   */
  buildDir: 'nuxt',
  build: {
    transpile: [/^vue2-google-maps($|\/)/],
    babel: {
      babelrc: true
    },
    extractCSS: true,
    publicPath: '/client/',
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            fix: true
          }
        })
      }
    }
  }
}
