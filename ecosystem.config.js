const pkg = require('./package')

module.exports = {
  apps: [{
    name: pkg.name, // App name that shows in `pm2 ls`
    exec_mode: 'cluster', // enables clustering
    instances: 'max', // or an integer
    max_memory_restart: '512M',
    // cwd: './current', // only if using a subdirectory
    script: './node_modules/nuxt-start/bin/nuxt-start.js', // The magic key
  }, {
    name: 'staging.' + pkg.name, // App name that shows in `pm2 ls`
    exec_mode: 'cluster', // enables clustering
    instances: 'max', // or an integer
    max_memory_restart: '512M',
    // cwd: './current', // only if using a subdirectory
    script: './node_modules/nuxt-start/bin/nuxt-start.js', // The magic key
  }],

  deploy: {
    production: {
      user: 'root',
      host: pkg.name,
      key: '~/.ssh/no_pass',
      ref: 'origin/master',
      repo: `git@bitbucket.org:aponomous/${pkg.name}.git`,
      path: `/home/${pkg.name}`,
      'post-deploy': `yarn && yarn build && NUXT_PORT=3000 pm2 startOrReload ecosystem.config.js --only ${pkg.name} && pm2 save && pm2 startup`
    },
    staging: {
      user: 'root',
      host: pkg.name,
      key: '~/.ssh/no_pass',
      ref: 'origin/staging',
      repo: `git@bitbucket.org:aponomous/${pkg.name}.git`,
      path: `/home/staging.${pkg.name}`,
      'post-deploy': `yarn && yarn build && NUXT_PORT=3001 pm2 startOrReload ecosystem.config.js --only staging.${pkg.name} && pm2 save && pm2 startup`
    }
  }
};
