export const state = () => ({
  isLoading: false,
  buildId: null
})

export const mutations = {
  SET_BUILD_ID(state, id) {
    state.buildId = id
  }
}

export const actions = {
  async nuxtServerInit({ commit }, { app, env, $axios }) {
    commit('SET_BUILD_ID', env.BUILD_ID)
    return
  }
}
