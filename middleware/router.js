export default function({
  isHMR,
  app,
  store,
  route,
  params,
  query,
  error,
  redirect
}) {
  // If middleware is called from hot module replacement, ignore it
  if (isHMR) return
}
