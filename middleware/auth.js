export default function({ store, redirect, app }) {
  if (!store.state.auth.token) {
    return redirect(
      app.localePath({
        name: 'register',
        query: {
          message: 'You need to login in order to proceed.'
        }
      })
    )
  }
}
